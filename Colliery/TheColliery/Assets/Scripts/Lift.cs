﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lift : MonoBehaviour
{

    public GameObject player;
    private PlayerController controller;
    private Rigidbody2D m_Rigidbody2D;
    private Vector3 m_Velocity = Vector3.zero;
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // How much to smooth out the movement


    [Header("Speeds")]
    public float MaxTravelSpeed = 40.0f;
    private float vertMove = 0.0f;
    public float InMotion = 0.0f;

    [Header("Stops")]
    public string CurrentStop = "Top";

    private void Awake()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
        controller = player.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        // Don't do anything if player is not in here
        if( controller.InLift && Mathf.Abs(InMotion) < float.Epsilon )
        {
            vertMove = Input.GetAxisRaw("Vertical") * MaxTravelSpeed;
            InMotion = vertMove;

        } else if( controller.InLift)
        {
            // But can change direction
            if (Mathf.Abs(Input.GetAxisRaw("Vertical")) > float.Epsilon)
            {
                if (Mathf.Sign(Input.GetAxisRaw("Vertical")) != Mathf.Sign(vertMove))
                {
                    InMotion = -InMotion;

                }
            }
            // Continue in same direction
            vertMove = InMotion;
        } else if( Mathf.Abs(InMotion) > float.Epsilon)
        {
            // Not in the lift and it's moving ... Oops
            // Just go down to the bottom
            vertMove = -MaxTravelSpeed;
            InMotion = vertMove;
        }
        // If current stop is top, you can't go up
        if( CurrentStop == "Top")
        {
            if( vertMove > 0.0f )
            {
                vertMove = 0.0f;
                InMotion = 0.0f;
            }
        }
        if( CurrentStop == "Bottom" )
        {
            if( vertMove < 0.0f)
            {
                vertMove = 0.0f;
                InMotion = 0.0f;
            }
        }
    }

    private void FixedUpdate()
    {
        Move(vertMove * Time.fixedDeltaTime);
    }
    
    private void Move(float move) 
    {
        Vector3 targetVelocity = new Vector2(m_Rigidbody2D.velocity.x, move * 10f);
        // And then smoothing it out and applying it to the character
        m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Collided with " + collision.name);
        if( collision.name != CurrentStop )
        {
            CurrentStop = collision.name;
            if (!controller.InLift)
            {
                // Lost our miner!

            }
            else
            {
                InMotion = 0.0f;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        CurrentStop = "";
    }
}
