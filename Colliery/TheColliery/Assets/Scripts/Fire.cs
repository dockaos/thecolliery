﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{

    public Animator animator;

    public void Extinquish()
    {
        SoundManager.Instance.PlayExtinquish();
        animator.SetBool("PutOut", true);
        Destroy(this.gameObject, 0.5f);
    }
}
