﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    private static UIManager _instance;
    public static UIManager Instance { get { return _instance; } }

    [Header("Screen Stuff")]
    public Text dogesSaved;
    public Text dogesValue;
    public Text CountDownTimer;
    public Canvas deathCanvas;
    public Canvas DoneCanvas;
    public Canvas RulesCanvas;

    [Header("Stats")]
    public Text TotalDoges;
    public Text SavedDoges;
    public Text BurnedDoges;
    public Text MoneyEarned;
    public Text TotalSavings;
    public Image DoneImage;

    [Header("Buttons")]
    public Button ButtonRestart;
    public Button ButtonOnward;

    public Image[] Holding;

    [Header("End Titles")]
    public Sprite BaBammSprite;
    public Sprite TimeOutSprite;
    public Sprite CompletedSprite;
    public GameObject PassText;
    public GameObject FailText;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        RulesCanvas.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        CountDownTimer.text = LevelManager.Instance.TimeLeft.ToString("F1");
        if( LevelManager.Instance.TimeLeft < 10.0f)
        {
            CountDownTimer.color = Color.red;
        }
    }

    // Update DogesSaved
    public void UpdateDogesSaved(int saved, int total)
    {
        dogesSaved.text = "Saved: " + saved + " OF " + total;
        dogesValue.text = "$" + (saved * GameManager.DogeCoinValue).ToString("F2");
    }

    public void ShowDeath()
    {
        deathCanvas.enabled = true;
    }

    public void RestartButton()
    {
        GameManager.Instance.Restart();
    }

    public void NextLevelButton()
    {
        GameManager.Instance.NextLevel();
    }

    public void ReturnToMenu()
    {
        GameManager.Instance.ReturnToMenu();
    }

    public void TimeOver()
    {
        Completed(true);
    }

    public void Completed(bool timeout=false)
    {
        if(timeout)
        {
            DoneImage.sprite = TimeOutSprite;
        } else
        {
            DoneImage.sprite = BaBammSprite;
        }
        if (GameManager.Instance.IsLastLevel())
        {
            DoneImage.sprite = CompletedSprite;
            ButtonRestart.gameObject.SetActive(true);
            ButtonOnward.gameObject.SetActive(false);
        }
        else
        {
            if (LevelManager.Instance.WonLevel)
            {
                ButtonRestart.gameObject.SetActive(false);
                ButtonOnward.gameObject.SetActive(true);

            }
            else
            {
                ButtonRestart.gameObject.SetActive(true);
                ButtonOnward.gameObject.SetActive(false);
            }
        }
        TotalDoges.text = LevelManager.Instance.TotalDoges.ToString();
        BurnedDoges.text = LevelManager.Instance.DogesBurned.ToString();
        SavedDoges.text = LevelManager.Instance.DogesSaved.ToString();
        MoneyEarned.text = (LevelManager.Instance.DogesSaved * GameManager.DogeCoinValue).ToString("F2");
        TotalSavings.text = GameManager.Instance.CoinValue().ToString("F2");
        DoneCanvas.enabled = true;
        GameObject result;
        if(LevelManager.Instance.WonLevel)
        {
            result = Instantiate(PassText, Vector3.zero, Quaternion.identity);
        } else
        {
            result = Instantiate(FailText, Vector3.zero, Quaternion.identity);
        }
        result.transform.SetParent(DoneCanvas.transform, false);
        result.transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 15.0f));

    }

    public void StartGame()
    {
        RulesCanvas.enabled = false;
        LevelManager.Instance.LevelStarted = true;
    }

    public void LoadLevel3()
    {
        SceneManager.LoadScene("Level03");
    }

    public void LoadLevel2()
    {
        SceneManager.LoadScene("Level02");
    }
}
