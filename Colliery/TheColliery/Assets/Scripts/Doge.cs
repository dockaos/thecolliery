﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doge : MonoBehaviour
{
    public PlayerController controller;
    private float horizMove = 0.0f;
    private Animator animator;

    public bool jump;
    public bool crouch;
    public bool IsDead;

    [Header("Movements")]
    public float ChangeTime = 2.0f;
    public float RunSpeed = 20.0f;

    private float timeLeft = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        controller.FacingRight = false;
        ChangeTime = Random.Range(1.0f, 3.0f);
    }

    // Update is called once per frame
    void Update()
    {
        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0)
        {
            // Change movements
            float stayOrGo = Random.Range(-1f, 1f);
            if( stayOrGo > 0)
            {
                animator.SetBool("Walking", true);
                horizMove = Random.Range(-1f, 1f) * RunSpeed;
            } else
            {
                horizMove = 0.0f;
                animator.SetBool("Walking", false);
            }
            timeLeft = ChangeTime;
        }

        if( IsDead )
        {
            Debug.Log("Doge is dead!");
            GameObject.Destroy(gameObject, 0.5f);
        }

        // If there is a bad thing in front of us, turn around
        // Doges are backwards :D
        Vector3 start;
        if( controller.FacingRight)
        {
            start = transform.position + new Vector3(0.5f, 0.0f, 0.0f);
        } else
        {
            start = transform.position + new Vector3(-0.5f, 0.0f, 0.0f);
        }
        RaycastHit2D badStuff = Physics2D.Raycast(start, controller.FacingRight ? transform.right : -transform.right, .25f, LayerMask.GetMask("Fire"));
        Debug.DrawRay(start, controller.FacingRight ? transform.right : -transform.right, Color.green);
        if ( badStuff.collider == true)
        {
            Debug.Log("Doge sees something! It's a " + badStuff.collider.name);
            if ( badStuff.collider.tag == "Fire")
            {
                
                // Turn around!
                horizMove = -horizMove;
            }
        }


    }

    private void FixedUpdate()
    {
        controller.Move(horizMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Fire")
        {
            animator.SetBool("Flambe", true);
            IsDead = true;
            LevelManager.Instance.DogeBurned();
        } else if( collision.tag == "Safe")
        {
            // Saved doge!
            SoundManager.Instance.PlayDogeSaved();
            LevelManager.Instance.DogeSaved();
            GameObject.Destroy(gameObject);
        }
    }
}
