﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private static LevelManager _instance;
    public static LevelManager Instance { get { return _instance; } }
    public int TotalDoges;
    public int DogesLeft;
    public int DogesBurned;
    public int DogesSaved;
    public int DogesToWin;

    public float TimeToPlay;
    public float TimeLeft;
    public bool TimeOut = false;
    public bool PlayerDead = false;
    public bool LevelCompleted = false;
    public bool LevelStarted = false;

    public bool WonLevel = false;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        DogesLeft = TotalDoges;
        DogesBurned = 0;
        DogesSaved = 0;
        TimeLeft = TimeToPlay;
        UIManager.Instance.UpdateDogesSaved(DogesSaved, TotalDoges);
    }

    // Update is called once per frame
    void Update()
    {
        if (!TimeOut && !PlayerDead && !LevelCompleted && LevelStarted)
        {
            TimeLeft -= Time.deltaTime;
            if (TimeLeft < 0.0f)
            {
                TimeLeft = 0.0f;
                TimeOut = true;
                LevelCompleted = true;
                GameManager.Instance.TotalCoins += DogesSaved;
                WonLevel = DogesSaved >= DogesToWin;
                UIManager.Instance.TimeOver();
            }
            CheckDone();
        }
    }

    public void CheckDone()
    {
        if( DogesBurned + DogesSaved == TotalDoges)
        {
            // Did it!
            LevelCompleted = true;
            WonLevel = DogesSaved >= DogesToWin;
            GameManager.Instance.TotalCoins += DogesSaved;
            UIManager.Instance.Completed();
            
        }
    }

    public void DogeBurned()
    {
        DogesBurned++;
        DogesLeft--;
    }

    public void DogeSaved()
    {
        DogesLeft--;
        DogesSaved++;
        UIManager.Instance.UpdateDogesSaved(DogesSaved, TotalDoges);
    }

    public void PlayerDied()
    {
        UIManager.Instance.ShowDeath();
        GameManager.Instance.PlayerDead();
        PlayerDead = true;
    }
}
