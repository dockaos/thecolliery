﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


// Game States
public enum GameState { INTRO, MAIN_MENU, INGAME, DEAD }

public class GameManager : MonoBehaviour
{

    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    public GameState gameState { get; private set; }

    public static float DogeCoinValue = 0.274283f;

    public float TotalCoins = 0.0f;

    public List<string> Levels = new List<string>();
    public int CurrentLevel = 0;
    public int StartingLevel = 0;



    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
   

    public void SetGameState(GameState state)
    {
        this.gameState = state;
        
    }

    public void OnApplicationQuit()
    {
        GameManager._instance = null;
    }

    public void Restart()
    {
        SetGameState(GameState.INGAME);
        TotalCoins = 0.0f;
        CurrentLevel = StartingLevel;
        SceneManager.LoadScene(Levels[StartingLevel]);
    }

    public void PlayerDead()
    {
        SetGameState(GameState.DEAD);
        TotalCoins += LevelManager.Instance.DogesSaved;
    }

    public void ReturnToMenu()
    {
        SetGameState(GameState.MAIN_MENU);
        TotalCoins = 0.0f;
        CurrentLevel = StartingLevel;
        SceneManager.LoadScene("MainMenu");
    }

    public float CoinValue()
    {
        return TotalCoins * DogeCoinValue;
    }

    public void NextLevel()
    {
        CurrentLevel++;
        SceneManager.LoadScene(Levels[CurrentLevel]);
    }

    public bool IsLastLevel()
    {
        if( SceneManager.GetActiveScene().name == Levels[Levels.Count-1])
        {
            return true;
        }
        return false;
    }
}
