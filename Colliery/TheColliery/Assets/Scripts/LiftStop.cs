﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftStop : MonoBehaviour
{

    public string Name;

    [Header("Triggers")]
    public BoxCollider2D upStop;
    public BoxCollider2D downStop;

    
}
