﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Pickupable
{
    public Sprite Image;
    public GameObject Prefab;
    public string Name;
}

public class PlayerPickups : MonoBehaviour
{

    public PlayerController controller;

    public List<Pickupable> Pickupables;
    public List<Pickupable> Holding;
    public int MaxItems = 2;
    public bool HasExtinquisher = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Holding.Count < MaxItems)
        {
            if (Input.GetButtonDown("Pickup"))
            {
                // Something pickupable around?
                Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.75f);
                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject.tag == "Pickupable")
                    {
                        PickUp(colliders[i].gameObject);
                        
                        break;
                    }
                }
            }
        }
        if( Holding.Count > 0 )
        {
            if( Input.GetButtonDown("Drop") )
            {
                // Drop it like it's hot
                Drop();
            }
        }
    }

    void PickUp(GameObject pObject)
    {
        Debug.Log("Picking up " + pObject.name);
        foreach(Pickupable pu in Pickupables)
        {
            if( pu.Name == pObject.name)
            {
                SoundManager.Instance.PlayYoink();
                int index = Holding.Count;
                Holding.Add(pu);
                UIManager.Instance.Holding[index].sprite = pu.Image;
                Destroy(pObject);
                if (pObject.name == "FireExtinquisher")
                {
                    HasExtinquisher = true;
                }
            }
        }
    }

    void Drop()
    {
        Pickupable toDrop = Holding[0];
        // Gone from holding
        Holding.RemoveAt(0);
        // If we have another object, shift the UI
        if (Holding.Count > 0)
        {
            UIManager.Instance.Holding[1].sprite = null;
            UIManager.Instance.Holding[0].sprite = Holding[0].Image;
        } else
        {
            UIManager.Instance.Holding[0].sprite = null;
        }
        // Instantiate Item in Front of Us
        float x = controller.FacingRight ? transform.position.x + 1.0f : transform.position.x - 1.0f;
        GameObject go = Instantiate(toDrop.Prefab, new Vector3(x, transform.position.y, 0.0f), transform.rotation);
        go.name = toDrop.Name;
        if( toDrop.Name == "FireExtinquisher")
        {
            HasExtinquisher = false;
        }

    }
}