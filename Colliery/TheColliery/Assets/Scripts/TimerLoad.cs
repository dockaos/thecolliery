﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class TimerLoad : MonoBehaviour
{

    public float TimeTillLoad = 5.0f;
    public VideoPlayer videoPlayer;

    // Start is called before the first frame update
    void Start()
    {
        videoPlayer.url = System.IO.Path.Combine(Application.streamingAssetsPath, "NotAPerson.mov");
        videoPlayer.Play();
    }

    // Update is called once per frame
    void Update()
    {
        TimeTillLoad -= Time.deltaTime;
        if( TimeTillLoad < 0.0f)
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

   
}
