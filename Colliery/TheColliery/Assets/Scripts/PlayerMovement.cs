﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public PlayerController controller;
    public PlayerPickups pickupsController;
    public Animator animator;

    private float horizMove = 0.0f;
    public float runSpeed = 40.0f;
    public bool jump = false;
    public bool crouch = false;
    public bool pickup = false;
    public bool drop = false;
    public bool IsDead = false;

    // Update is called once per frame
    void Update()
    {
        if( IsDead )
        {
            return;
        }
        horizMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        if( Mathf.Abs(horizMove) > 0.0f)
        {
            animator.SetBool("Walking", true);
        } else
        {
            animator.SetBool("Walking", false);
        }
        animator.SetBool("HasFireExtinquisher", pickupsController.HasExtinquisher);
        if(Input.GetButtonDown("Jump"))
        {
            jump = true;
        }
        if(Input.GetButtonDown("Crouch"))
        {
            crouch = true;
        }
        else if(Input.GetButtonUp("Crouch"))
        {
            crouch = false;
        }
    }

    void FixedUpdate()
    {
        if (IsDead || LevelManager.Instance.LevelCompleted || ! LevelManager.Instance.LevelStarted)
        {
            horizMove = 0.0f;
            return;
        }
        controller.Move(horizMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Fire")
        {
            if (pickupsController.HasExtinquisher)
            {
                collision.GetComponent<Fire>().Extinquish();
            }
            else
            {
                //animator.SetBool("Flambe", true);
                IsDead = true;
                animator.SetBool("OnFire", true);
                LevelManager.Instance.PlayerDied();
                Destroy(this.gameObject, 1f);
            }
        }
    }
}
