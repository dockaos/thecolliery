﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SoundManager : MonoBehaviour
{

    private static SoundManager _instance;
    public static SoundManager Instance { get { return _instance; } }

    public AudioClip yoink;
    public AudioClip burnup;
    public AudioClip dogeSaved;

    public AudioSource soundEffects;
    public AudioSource musicSource;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeBackgroundMusic(AudioClip newClip)
    {
        musicSource.clip = newClip;
    }

    public void PlayYoink()
    {
        soundEffects.PlayOneShot(yoink);
    }

    public void PlayExtinquish()
    {
        soundEffects.PlayOneShot(burnup);
    }

    public void PlayDogeSaved()
    {
        soundEffects.PlayOneShot(dogeSaved);
    }
}
